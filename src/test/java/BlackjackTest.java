package test.java;

import static org.junit.jupiter.api.Assertions.*;


import java.util.ArrayList;

import main.java.Blackjack;
import main.java.Card;
import main.java.Deck;
import main.java.Suit;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.RepeatedTest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class BlackjackTest {
	Deck deck = new Deck();
	@BeforeEach
	void newDeck() {
		Deck deck = new Deck();
	}
	
	@Test
	void deckExistTest() {
		assertNotNull(deck);
	}
	
	@Test
	void deckSizeTest() {
		int size = deck.getCards().size();
		deck.shuffle();
		assertEquals(size,deck.getCards().size(),"Deck size equal before and after shuffle");
	}
	
	@Test
	void deckSameSizeAfterDrawTest() {
		Deck deck = new Deck();//Vet inte varför BeforeEach inte funkar här
		deck.draw();
		int size = deck.getCards().size();
		deck.shuffle();
		assertEquals(size,deck.getCards().size(),"Draw card, compare deck size before and after shuffle.");
	}
	
	@Test
	void deckCompareTwoCardsToBeNotSame() {
		while(deck.getCards().size() > 0) {
			Card temp = deck.draw();
			for(int i = 0; i < deck.getCards().size(); i++)
				assertFalse(temp.equals(deck.getCards().get(i)),"Draw two cards to be not the same");
		}
	}
	
	@Test
	void deckCompareTwoCardsToBeNotSameAfterShuffle() {
		deck.shuffle();
		while(deck.getCards().size() > 0) {
			Card temp = deck.draw();
			for(int i = 0; i < deck.getCards().size(); i++)
				assertFalse(temp.equals(deck.getCards().get(i)),"Shuffle and draw two cards to be not the same");
		}
	}
	@Test
	void cardOverrideEqualsTest() {
		Card card1 = new Card(1, Suit.HEARTS);
		Card card2 = new Card(1,Suit.HEARTS);
		assertTrue(card1.equals(card2));
	}
	@RepeatedTest(52)
	void deck() {
		Deck deck = new Deck();//Samma här. Vet ej om BeforeEach endast körs en gång innan ett repeatedTest eller om den körs varje repition.
		deck.draw();
	}
	
	@ParameterizedTest
	@ValueSource(ints={2,4,10})
	void deckTryScoreThreeCards(int n) {
		Blackjack blackjack = Blackjack.getInstance();
		ArrayList<Card> list = new ArrayList<>();
		for(int i = 0; i < 3; i++) {
			list.add(new Card(n,Suit.HEARTS));
		}
			assertEquals(n*3, blackjack.calculate(list));
	}

}
