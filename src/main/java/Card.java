package main.java;

public class Card {
    private int value;
    private Suit suit;

    public int getValue() {
        return value;
    }

    public Suit getSuit() {
        return suit;
    }

    public String getSuitString(){
        String temp=suit.toString();
        return temp.substring(0,1).toUpperCase()+temp.toLowerCase().substring(1);
    }

    public Card(int value, Suit suit) {
        this.value = value;
        this.suit = suit;
    }

    @Override
    public boolean equals(Object o){
        return ((Card)o).getValue()== this.getValue() && ((Card)o).getSuit() == this.getSuit();
    }
}
