package main.java;

public enum Suit {
    HEARTS, SPADES, DIAMONDS, CLUBS;
}
