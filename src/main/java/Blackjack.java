package main.java;

import java.util.List;

public class Blackjack {
    private static final Blackjack instance = new Blackjack();
    private Player player = new Player();
    private Player dealer = new Player();
    private Deck deck = new Deck();

    private Blackjack() {
    }

    public Player getPlayer() {
        return player;
    }

    public static Blackjack getInstance(){
        return instance;
    }
    public void hit(){
        newCard(player);
        if (player.getScore()>21) {
            printCards(true);
            System.out.println("Bust.");
        }
    }
    public void stand(){
        while (dealer.getScore()<17) {
            dealer();
        }
        printCards(true);
        if(player.getScore() > dealer.getScore()||dealer.getScore()>21){
            System.out.println("You beat the dealer.");
        }else if(player.getScore() == dealer.getScore()){
            System.out.println("Draw.");
        }else {
            System.out.println("Dealer wins.");
        }
    }

    public void reset(){
        deck = new Deck();
        deck.shuffle();
        player.clearHand();
        dealer.clearHand();
        player.setScore(0);
        dealer.setScore(0);
        newCard(player);
        newCard(player);
        newCard(dealer);
        newCard(dealer);
    }
    public void dealer(){
        if (dealer.getScore()<17)
            newCard(dealer);
    }
    private void newCard(Player player){
        player.addCard(deck.draw());
        player.setScore(calculate(player.getHand()));
    }
    public int calculate(List<Card> cards){
        boolean ace=false;
        int score = 0;
        for (Card c : cards){//Check for ace
            int value = c.getValue();
            if (value==1 && !ace) {
                ace = true;
            }else if (value==1 && ace){
                score += 1;
            }else if (value > 10)
                score += 10;
            else {
                score += value;
            }
        }
        if (ace){
            if (score+11 > 21){
                score+=1;
            }
            else{
                score+=11;
            }
        }
        return score;
    }

    public void printCards(boolean end){//Boolean:End of game? true: print all cards.
        System.out.print("Player: ");
        print(player.getHand());
        System.out.println(" Total: "+player.getScore());
        System.out.print("Dealer: ");
        if (!end){//First card face down
            System.out.print("Unknown, ");
            print(dealer.getHand().subList(1,dealer.getHand().size()));
            System.out.println();
        }else{
            print(dealer.getHand());
            System.out.println(" Total: "+dealer.getScore());
        }
    }
    private void print(List<Card> cards){
        for (Card c : cards) {
            System.out.format("%s %s, ", readRank(c), c.getSuitString());
        }
    }


    private String readRank(Card card){
        switch (card.getValue()) {
            case 1:return "Ace of";
            case 11:return "Jack of";
            case 12:return "Queen of";
            case 13:return "King of";
        }
        return String.valueOf(card.getValue());
    }
}


