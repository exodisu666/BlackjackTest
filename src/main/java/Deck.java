package main.java;
//MAVEN TESTING ALL IS SHIIIIIT

import java.util.ArrayList;

public class Deck {
    private ArrayList<Card> cards;

    public Card draw(){
        Card temp = cards.get(0);
        cards.remove(0);
        return temp;
    }
    public Deck(){
        newDeck();
        //shuffle();
    }
    public void shuffle(){
        ArrayList<Card> temp = new ArrayList<>();
        while(!cards.isEmpty()) {
            int loc=(int)(Math.random()* cards.size());
            temp.add(cards.get(loc));
            cards.remove(loc);
        }
        cards =temp;
    }
    private void newDeck() {
        cards = new ArrayList<>();
        for (int i = 0; i < 4;i++){
            for (int j = 1;j<= 13;j++){
                this.cards.add(new Card(j,Suit.values()[i]));
            }
        }
    }
    public ArrayList<Card> getCards() {
		return cards;
	}
    
    
    
	public static boolean testShuffle(Deck temp){
        int i = temp.cards.size();
        temp.shuffle();
        return i==temp.cards.size();
    }
    public static boolean testShuffleDraw(){
        Deck deck = new Deck();
        if(testShuffle(deck)){
        deck.draw();
            if(!testShuffle(deck)){
                return false;
            }
        }
        return true;
    }
    public static boolean testEqualCard(){
        Deck deck = new Deck();

        for (int i = 0; i < deck.cards.size()-1;i++){
            for (int j = i+1; j < deck.cards.size();j++){
                if(deck.cards.get(i).equals(deck.cards.get(j))){
                    return false;
                }
            }
        }
        deck.shuffle();
        for (int i = 0; i < deck.cards.size()-1;i++){
            for (int j = i+1; j < deck.cards.size();j++){
                if(deck.cards.get(i).equals(deck.cards.get(j))){
                    return false;
                }
            }
        }
        return true;
    }
    public static boolean testequal(){
        Deck deck = new Deck();
        if(deck.cards.get(0).equals(deck.cards.get(0))) {
            return true;
        }
        return false;
    }
}
