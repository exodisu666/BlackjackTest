package main.java;

import java.util.ArrayList;
import java.util.List;

public class Player {
    private int score=0;
    private List<Card> hand = new ArrayList<>();

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public List<Card> getHand() {
        return hand;
    }

    public void addCard(Card card){
        this.hand.add(card);
    }
    public void clearHand(){
        this.hand.clear();
    }
}
