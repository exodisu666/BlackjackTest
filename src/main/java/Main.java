package main.java;


import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		String testInput;
		Blackjack blackjack = Blackjack.getInstance();
		blackjack.reset();
		while (true) {
			while (blackjack.getPlayer().getScore() <= 21) {
				blackjack.printCards(false);// Bool: End of game?
				System.out.print("Hit? y/n: ");
				testInput = input.next();
				while (testInput == null) {
					testInput = validator(input.next());
				}
				if (testInput.equals("y")) {
					blackjack.hit();
				} else {
					blackjack.stand();
					break;
				}

			}
			System.out.print("New game? y/n: ");
			testInput = input.next();
			while (testInput == null) {
				testInput = validator(input.next());
			}
			if (testInput.equals("y"))
				blackjack.reset();
			else
				System.exit(0);
		}
	}

	public static String validator(String input) {
		if (input.toLowerCase().equals("y") || input.toLowerCase().equals("n"))
			return input;
		else {
			System.out.println("Invalid input. Only y or n");
			return null;
		}
	}
}
